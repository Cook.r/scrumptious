# Scrumptious



## Getting started

Scrumptious is a django application please run it in your local host. In the application's parent directory please enter "python manage.py runserver"

## Application Details

Scrumptious was created to help users manage recipes they have. This application allows users to create and edit recipes, users can also add steps and ingredients into their recipes.

This django project has two apps inside one for the accounts which allows users to sign up and login and the other contains all the code for all the functionality of the recipes. 


To allow users to add steps and ingredients to the recipe, I modeled my data using recipe as a foreign key for my RecipeStep model and my Ingredients Model. 

Also please note that the picture field on my Recipe model is a urlfield meaning that when making a recipe please be sure that the image link is not over 200 characters long.


<h1> Please see examples of below </h1>


<h3> This form allows users to create recipes by providing a name, image url, description and a rating. Please not that the image url has to be a valid URL. <h3>

![alternate text](images/create_recipe.png)

<h3>Users can see the recipe they just made appear in the list of recipes by clicking the home button.  </h3>

![alternate text](images/main_page.png)


<h3> From the home page users can click on the name of their recipe and view all the details about that particular recipe. Users can also add steps and ingredients to the recipe from the detail page.  </h3>

![alternate text](images/recipe_details.png)

<h3> If users want to edit their recipe this option can also be viewed from the detail page. The placeholders are set to the vales of that particular recipe </h3>

![alternate text](images/Edit_recipe.png)

<h3> Users can sign up and login to view recipes they have personally added or to create new ones. <h3>

![alternate text](images/Signup.png)



![alternate text](images/Login.png)



