from django.shortcuts import render , get_object_or_404, get_list_or_404, redirect
from django.http import HttpResponse
from recipes.models import Recipe, RecipeStep, Ingredients
from recipes.forms import RecipeForm,EditRecipeForm, EditStepsForm, EditIngredientForm
from django.contrib.auth.decorators import login_required

# Create your views here.

# watch indentation 

@login_required
def create_recipe(request):
    if request.method=="POST":
        print(request.POST)
        form = RecipeForm(request.POST)
        if form.is_valid():
            print(form)
            recipe=form.save(False)
            recipe.author=request.user
            form.save()
            return redirect("recipe_list")
    else:
        form =RecipeForm()
    context={
        "form":form
            }
    return render(request,"recipes/create.html",context)


def delete_recipe(request, id):
  list = get_object_or_404(Recipe.objects, id=id)
  if request.method == "POST":
    list.delete()
    return redirect("recipe_list")

  return render(request, "recipes/recipe_delete.html")



def edit_recipe(request,id):
    post =get_object_or_404(Recipe.objects,id=id)
    if request.method == "POST":
        form = EditRecipeForm(request.POST, instance=post)
        if form.is_valid():
            form.save()
            return redirect("show_recipe",id=id)
    else:
        form =EditRecipeForm(instance=post)

    context={
        "post_object":post,
        "post_form":form,
    }
    return render(request, "recipes/edit.html",context)


def add_steps(request,id):
    recipes = Recipe.objects.filter(id=id)
    if request.method=="POST":
        print(request.POST)
        form = EditStepsForm(request.POST)
        if form.is_valid():
            recipe=form.save(False)
            recipe.author=request.user
            form.save()
            return redirect("recipe_list")
    else:
        form =EditStepsForm()
    context={
        "recipes":recipes,
        "form":form
        }
    return render(request,"recipes/add_steps.html",context)


def add_ingredients(request):
    if request.method=="POST":
        form = EditIngredientForm(request.POST)
        if form.is_valid():
            recipe=form.save(False)
            recipe.author=request.user
            form.save()
            return redirect("recipe_list")
    else:
        form =EditIngredientForm()
    context={
        "form":form
        }
    return render(request,"recipes/add_ingredients.html",context)


def show_recipe(request,id):
    # recipe=Recipe.objects.get(id=id)
    recipe=get_object_or_404(Recipe.objects,id=id)
    context={
        "recipe_object":recipe
    }
    return render(request, "recipes/detail.html", context)

def recipe_list(request):
    recipes= get_list_or_404(Recipe.objects.all())
    context={
        "recipe_list":recipes
    }
    return render(request , "recipes/list.html", context)

@login_required
def my_recipe_list(request):
    recipes = Recipe.objects.filter(author=request.user)
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/mine.html", context)
