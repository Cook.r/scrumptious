from django.contrib import admin
from recipes.models import Recipe,RecipeStep,Ingredients



@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list_display=[
        "title",
        "id",
        "rating"
    ]
    



@admin.register(RecipeStep)
class RecipeStepAdmin(admin.ModelAdmin):
    list_display=[
        "step_number",
        "id",
        "instruction"
    ]


@admin.register(Ingredients)
class RecipeIngredientAdmin(admin.ModelAdmin):
    list_display=[
        "amount",
        "food_item"
    ]
