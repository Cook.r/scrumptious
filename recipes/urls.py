from django.urls import path
from recipes.views import show_recipe, recipe_list,create_recipe,edit_recipe,my_recipe_list,add_steps, add_ingredients, delete_recipe


urlpatterns = [
    path('recipes/<int:id>/', show_recipe, name="show_recipe"),
    path("recipes/",recipe_list, name ="recipe_list"),
    path("recipes/create/", create_recipe, name="create_recipe"),
    path("recipes/<int:id>/edit/", edit_recipe, name ="edit_recipe"),
    path("mine/",my_recipe_list, name="my_recipe_list"),
    path("recipes/add_steps/<int:id>", add_steps, name="add_steps"),
    path("recipes/ingredients/", add_ingredients, name="add_ingredients"),
    path("recipes/delete/<int:id>", delete_recipe, name="delete_recipe"),


]
