from django.forms import ModelForm
from recipes.models import Recipe, RecipeStep, Ingredients


class RecipeForm(ModelForm):
    class Meta:
        model=Recipe
        fields=[
            "title",
            "picture",
            "description",
            "rating"
        ]

class EditRecipeForm(ModelForm):
    class Meta:
        model=Recipe
        fields=[
            "title",
            "picture",
            "description",
            "rating"
        ]

class EditStepsForm(ModelForm):
    class Meta:
        model=RecipeStep
        fields=[
            "step_number",
            "instruction",
            "recipe"
        ]

class EditIngredientForm(ModelForm):
    class Meta:
        model = Ingredients
        fields =[
            "amount",
            "food_item",
            "recipe"
        ]
